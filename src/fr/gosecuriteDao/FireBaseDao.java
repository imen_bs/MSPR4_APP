package fr.gosecuriteDao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

public class FireBaseDao {
	
	
	
	
private Firestore db;
public FireBaseDao(String contextPath) {
	try {
				
				FileInputStream serviceAccount =
				  new FileInputStream(contextPath+"/key.json");

				FirebaseOptions options = new FirebaseOptions.Builder()
				  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
				  .build();

				FirebaseApp.initializeApp(options);
	}
	catch(FileNotFoundException e)
	{
		e.printStackTrace();
	}
	catch(IOException e)
	{
		e.printStackTrace();
	}
	}

public Firestore getDbConnexion()
{if (this.db == null)
	this.db = FirestoreClient.getFirestore();
return this.db;
	}

}
	
		